/**
 * Created by MShipman on 25/11/2016.
 */
var raffleController = new (require("./raffle_controller"));

// Enum for command access levels
const ACCESS_LEVEL = {
    NORMAL: 0,
    MOD: 1
};

// List of commands
const commands = {
    "!openraffle" : {"function": raffleController.open, "access_level": ACCESS_LEVEL.MOD},
    "!closeraffle" : {"function": raffleController.close, "access_level": ACCESS_LEVEL.MOD},
    "!raffle" : {"function": raffleController.enter, "access_level": ACCESS_LEVEL.NORMAL},
    "!drawraffle" : {"function": raffleController.draw, "access_level": ACCESS_LEVEL.MOD},
    "!resetraffle" : {"function": raffleController.reset, "access_level": ACCESS_LEVEL.MOD}
};

/**
 * Default constructor
 */
var controller = function(){};

/**
 * Function that is called to interpret commands and process them
 *
 * @param client
 * @param channel
 * @param sender
 * @param message
 */
controller.prototype.parseCommand = function(client, channel, sender, message) {

    var args = message.split(' ');
    var commandStr = args.splice(0, 1)[0]; // Remove first arg and store as commandStr
    var params = args; // Every arg after commandStr

    // The command object that contains the function to run
    var command = commands[commandStr];

    // Check that the command is valid
    if(command === undefined) {
        return; // No such commands
    }

    var accessLevel = command.access_level;

    // Check that the sender has the correct access level to te command
    if(!hasAccessLevel(sender, accessLevel)) {
        return; // Access level not granted
    }

    var functionPtr = command.function; // function to fire for this command

    // Check that the function is no undefined
    if(functionPtr === undefined) {
        return; // No function for this command
    }

    // Call the function and pass all the needed data
    functionPtr(params, client, channel, sender, message);

};

/**
 * Function used to check that the user has needed access level
 *
 * @param sender
 * @param accessLevel
 * @returns {boolean}
 */
var hasAccessLevel = function(sender, accessLevel) {

    if(accessLevel === ACCESS_LEVEL.NORMAL) {
        return true; // All users have normal or above access level
    }

    // TODO: Add mod access level check

    // TODO: Remove when mode check is implemented
    else if(sender === 'mshipman') {
        return true;
    }

    return false;

};

module.exports = controller;