/**
 * Created by MShipman on 25/11/2016.
 */
var ircClient = require("node-irc");
var clientConfig = require("../configs/client");
var commandController = new (require("./command_controller"));

// Constants
const TWITCH_URL = 'irc.chat.twitch.tv';
const IRC_PORT = 6667;

// Values for Twitch username, auth token and channel
// Auth token can be obtained from http://www.twitchapps.com/tmi/
var username = clientConfig.twitchUsername;
var authToken = clientConfig.twitchAuthToken;
var channel = clientConfig.twitchChannel;

// new ircClient(server, port, nickname, fullname, password)
var client = new ircClient(TWITCH_URL, IRC_PORT, username, username, authToken);

/**
 * Function that is fired when the client is ready and running
 */
client.on('ready', function() {

    client.join(channel + " " + authToken); // Join the specified channel
    // client.say(channel, "ShippyBot has entered the chat!"); // Say a message to the chat when the bot is connected

});

/**
 * Function that is fired when any message is received
 *
 * The data object contains:
 *      data.receiver : The channel the person joined, prefixed by a hash (#)
 *      data.sender   : The nick of the person who joined
 *      data.message  : The message sent from the users
 *
 */
client.on('CHANMSG', function (data) {

    // Undefined check (should never be fired but used just for safety)
    if(data.receiver === undefined || data.sender === undefined || data.message === undefined) {
        console.error("CHANMSG Error: receiver, sender or message was undefined");
        return;
    }

    // Don't process messages sent from this bot
    if(data.sender !== username) {
        commandController.parseCommand(client, data.receiver, data.sender, data.message)
    }

});

// Connect once everything is setup for the client
client.connect();