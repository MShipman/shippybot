/**
 * Created by MShipman on 26/11/2016.
 */

// Success messages
const RAFFLE_OPENED_MESSAGE = "A new raffle has been opened.";
const RAFFLE_CLOSED_MESSAGE = "The raffle has now been closed.";
const RAFFLE_RESET_MESSAGE = "The raffle was reset.";

// Error messages
const ALREADY_OPENED_ERROR = "There is already an open raffle.";
const NO_OPENED_RAFFLE_ERROR = "There is no open raffle.";
const RAFFLE_NOT_CLOSED_ERROR = "The raffle is not yet closed.";
const EMPTY_RAFFLE_ERROR = "The raffle is empty.";

// Params
const MINUTES_PARAM = "minutes:";
const DRAWS_PARAM = "draws:";

var users = undefined;
var open = false;

/**
 * Default constructor
 */
var controller = function(){};

/**
 * Function that inits/opens a raffle
 *
 * @param params
 * @param client
 * @param channel
 * @param sender
 * @param message
 * @param callback
 * @returns {*}
 */
controller.prototype.open = function(params, client, channel, sender, message) {

    // Check if there is already a raffle open
    if(open) {
        client.say(channel, ALREADY_OPENED_ERROR);
        return;
    }

    var minutes = 0;
    var draws = 0;

    // Undefined check params
    if(params !== undefined) {

        // Check params
        params.forEach(function(param) {

            if(param.startsWith(MINUTES_PARAM))
                minutes = parseInt(param.substring(MINUTES_PARAM.length));

            else if(param.startsWith(DRAWS_PARAM))
                draws = parseInt(param.substring(DRAWS_PARAM.length));

        });

    }

    if(minutes !== undefined && minutes !== 0) {

        return scheduleRaffle(client, channel, minutes, draws);

    }

    // Open up a new raffle and init the user array
    open = true;
    users = [];

    client.say(channel, RAFFLE_OPENED_MESSAGE);
};

/**
 * Function used for closing a raffle
 *
 * @param params
 * @param client
 * @param channel
 * @param sender
 * @param message
 * @param callback
 * @returns {*}
 */
controller.prototype.close = function(params, client, channel, sender, message) {

    // Check that there is an opened raffle
    if(!open) {
        client.say(channel, NO_OPENED_RAFFLE_ERROR);
        return;
    }

    var draws = 0;

    // Undefined check params
    if(params !== undefined) {

        // Check params
        params.forEach(function(param) {

            if(param.startsWith(DRAWS_PARAM))
                draws = parseInt(param.substring(DRAWS_PARAM.length));

        });

    }

    // Close the raffle
    open = false;

    client.say(channel, RAFFLE_CLOSED_MESSAGE);

    // Check if auto draw is needed
    if(draws !== undefined && draws > 0) {

        while(draws > 0 && users.length > 0) {

            controller.prototype.draw(undefined, client, channel, undefined, undefined);
            draws--;

        }

    }
};

/**
 * Function used for drawing users from the last closed raffle
 *
 * @param params
 * @param client
 * @param channel
 * @param sender
 * @param message
 * @param callback
 * @returns {*}
 */
controller.prototype.draw = function(params, client, channel, sender, message) {

    // Check that raffle is not still open
    if(open) {
        client.say(channel, RAFFLE_NOT_CLOSED_ERROR);
        return;
    }

    // Check that there is people in the raffle
    if(users === undefined || users.length === 0) {
        client.say(channel, EMPTY_RAFFLE_ERROR);
        return;
    }

    // Draw a user
    var user = users[Math.floor(Math.random() * users.length)];
    var message = user + " has won the raffle!";

    // Get the index of the winning user and remove them from the raffle
    var index = users.indexOf(user);
    users.splice(index, 1);

    // Announce winner
    client.say(channel, message);
};

/**
 * Function to reset/clear the raffle
 *
 * @param params
 * @param client
 * @param channel
 * @param sender
 * @param message
 * @param callback
 * @returns {*}
 */
controller.prototype.reset = function(params, client, channel, sender, message) {

    // Check that there is an open raffle
    if(!open) {
        client.say(channel, NO_OPENED_RAFFLE_ERROR);
        return;
    }

    // Empty out users
    users = [];

    client.say(channel, RAFFLE_RESET_MESSAGE);
};

/**
 * Function to allow users to enter the raffle
 *
 * @param params
 * @param client
 * @param channel
 * @param sender
 * @param message
 * @param callback
 */
controller.prototype.enter = function(params, client, channel, sender, message) {

    if(!open) {
        client.say(channel, NO_OPENED_RAFFLE_ERROR);
        return;
    }

    // Search if user is already in raffle
    var found = false;
    for (var i = 0; i < users.length && !found; i++) {
        if (users[i] === sender) {
            found = true;
        }
    }

    // Return if the user is already in the raffle
    if(found) {
        client.say(channel, sender + " is already in the raffle.");
        return;
    }

    // Add the user to the raffle
    users.push(sender);

    client.say(channel, sender + " was added to the raffle.");


};

/**
 * Function that automatically handles a raffle depending on the params
 *
 * @param client
 * @param channel
 * @param minutes
 * @param draws
 */
var scheduleRaffle = function(client, channel, minutes, draws) {

    // Check if there is already a raffle open
    if(open) {
        client.say(channel, ALREADY_OPENED_ERROR);
        return;
    }

    // Open up a new raffle
    controller.prototype.open(undefined, client, channel, undefined, undefined);

    // Consts for countdowns
    const minuteInMilliseconds = 60000;
    const tenSecondsInMilliseconds = 10000;
    const secondInMilliseconds = 1000;

    var milliseconds = minutes * minuteInMilliseconds;
    var leftTillAMinute = milliseconds - minuteInMilliseconds;
    leftTillAMinute = leftTillAMinute < 0 ? 0 : leftTillAMinute;

    client.say(channel, "The raffle will close in " + minutes * 60 + " seconds.");

    // Countdown to 60 seconds
    setTimeout(function() {

        if(!open) {
            return; // Raffle was closed
        }

        client.say(channel, "60 seconds remaining on the raffle.");

        // Countdown to 10 seconds
        setTimeout(function() {

            if(!open) {
                return; // Raffle was closed
            }

            var secondsLeft = 10;
            client.say(channel, "" + secondsLeft);

            // Countdown from 10 to 0 seconds
            var finalCountdown = setInterval(function() {

                if(!open) {
                    return; // Raffle was closed
                }

                secondsLeft--;

                if(secondsLeft == 0) {
                    clearInterval(finalCountdown); // Clear the interval

                    var params = ["draws:" + draws];
                    controller.prototype.close(params, client, channel, undefined, undefined);
                }
                else {
                    client.say(channel, secondsLeft.toString());
                }

            }, secondInMilliseconds); // 10 - 0 seconds

        }, minuteInMilliseconds - tenSecondsInMilliseconds); // 10 seconds left

    }, leftTillAMinute); // 60 seconds left

};

module.exports = controller;