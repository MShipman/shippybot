/**
 * Bot can be started directly from irc_controller but app.js was kept for convention sake
 */

// This will start the bot
require("./bot/controllers/irc_controller");